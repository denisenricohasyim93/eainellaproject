const soapRequest = require ('easy-soap-request');
var moment = require('moment')
// SendSMS
async function SendSMSGagalRenewal (msisdn, message, trx_id) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
  };

  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                      <v1:SendSMSReminderWoMRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/CPWithoutMirroring/SendSMSReminderWoMRq/v1.0">
                          <!--Optional:-->
                          <v1:trx_id></v1:trx_id>
                          <v1:msisdn>${msisdn}</v1:msisdn>
                          <v1:smsMessage>${message.split('<spasi>').pop()}</v1:smsMessage>
                      </v1:SendSMSReminderWoMRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']
    );
  });
  var kiriman =
    msisdn + '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + message + '|' + 'Failed';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/SendSMSFailedLogs_' + moment ().format ('DD-MMM-YY') + '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );
}

// SendSMS
async function SendSMS (msisdn, message, trx_id) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
  };

  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                      <v1:SendSMSReminderWoMRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/CPWithoutMirroring/SendSMSReminderWoMRq/v1.0">
                          <!--Optional:-->
                          <v1:trx_id></v1:trx_id>
                          <v1:msisdn>${msisdn}</v1:msisdn>
                          <v1:smsMessage>${message.split('<spasi>').pop()}</v1:smsMessage>
                      </v1:SendSMSReminderWoMRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']
    );
  });
  var kiriman =
    msisdn + '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + message + '|' + 'Success';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/SendSMSucceedRenewalLogs_' + moment ().format ('DD-MMM-YY') + '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );
}
// ChargeToINGW
async function ChargeToINGW (item) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/ChargeProxyService',
  };
  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                      <v1:ChargeRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/AutoRenewalProducts/ChargeRq/v1.0">
                          <v1:MSISDN>${item.MSISDN[0]}</v1:MSISDN>
                          <v1:TRX_ID>${item.TRX_ID[0]}</v1:TRX_ID>
                          <v1:ORDER_ID></v1:ORDER_ID>
                          <v1:APPS_ID></v1:APPS_ID>
                          <v1:CP_NAME></v1:CP_NAME>
                          <v1:CP_PASSWORD></v1:CP_PASSWORD>
                      </v1:ChargeRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/ChargeProxyService',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']    );
  });
  var kiriman =
    item.MSISDN[0] + '|'+ '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + 'Success';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/AutoRenewalProductChargeToINGWLogs_' +
      moment ().format ('DD-MMM-YY') +
      '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );

  return {
    status: 200,
    message: 'Berhasil Charging To INGW',
  };
}
// CreateMemoToAOM
async function CreateMemoToAOM (item) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/CreateMemoToAOMProxyService',
  };
  var MEMO_TEXT = 'Created by EaiScheduler';
  var MEMO_TYPE_ID = '90005';
  var SECURED_TICKET = 'SECURED ONE';
  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                        <v1:l9CreateMemoByMsisdn xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/AutoRenewalProducts/CreateMemoRq/v1.0">
                            <!--Optional:-->
                            <v1:cm9MemoInfo>
                                <!--Optional:-->
                                <v1:memoManualText>${MEMO_TEXT}</v1:memoManualText>
                                <!--Optional:-->
                                <v1:msisdn>${item.MSISDN[0]}</v1:msisdn>
                                <!--Optional:-->
                                <v1:memoTypeId>${MEMO_TYPE_ID}</v1:memoTypeId>
                            </v1:cm9MemoInfo>
                            <!--Optional:-->
                            <v1:_awsi_header>
                                <!--Optional:-->
                                <v1:securedTicket>${SECURED_TICKET}</v1:securedTicket>
                            </v1:_awsi_header>
                        </v1:l9CreateMemoByMsisdn>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/CreateMemoToAOMProxyService',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']    );
  });
  var kiriman =
     MEMO_TYPE_ID + '|' + MEMO_TEXT + '|' + item.MSISDN[0] + '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + 'Success';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/AutoRenewalProductCreateMemoToAOMLogs_' +
      moment ().format ('DD-MMM-YY') +
      '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );
}
// AddRemoveOffer
async function AddRemoveOffer (item) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/SendRemoveOfferToAOM',
  };
  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                      <v1:SendRemoveOfferToAOMRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/AutoRenewalProducts/resource/SendRemoveOfferToAOMRq/v1.0">
                            <v1:OrderingContext>
                                <v1:SalesChannel>A</v1:SalesChannel>
                                <v1:LocaleString>A</v1:LocaleString>
                                <!--Optional:-->
                                <v1:SecurityToken>A</v1:SecurityToken>
                            </v1:OrderingContext>
                            <v1:ExternalProductID></v1:ExternalProductID>
                            <!--Optional:-->
                            <v1:ExternalProductOrderID></v1:ExternalProductOrderID>
                            <!--Optional:-->
                            <v1:ConfigurationAction>
                                <v1:ActionType>
                                    <v1:Value>DELETE_OFFER</v1:Value>
                                </v1:ActionType>
                                <v1:ItemCode>123</v1:ItemCode>
                                <!--Optional:-->
                                <v1:ItemInstanceId></v1:ItemInstanceId>
                                <v1:AttributeList>
                                    <!--Zero or more repetitions:-->
                                    <v1:Attribute>
                                        <v1:Name></v1:Name>
                                        <v1:Value></v1:Value>
                                        <!--Optional:-->
                                        <v1:AttributeDataType>
                                            <v1:ValueType></v1:ValueType>
                                        </v1:AttributeDataType>
                                    </v1:Attribute>
                                </v1:AttributeList>
                            </v1:ConfigurationAction>
                            <!--Optional:-->
                            <v1:ReasonCode></v1:ReasonCode>
                            <!--Optional:-->
                            <v1:ReasonText></v1:ReasonText>
                            <!--Optional:-->
                            <v1:callBackURL></v1:callBackURL>
                            <!--Optional:-->
                            <v1:ServiceFilterInfo>
                                <v1:FieldName></v1:FieldName>
                                <v1:Value></v1:Value>
                                <!--Optional:-->
                                <v1:ValueDataType></v1:ValueDataType>
                            </v1:ServiceFilterInfo>
                            <!--Optional:-->
                            <v1:ImplementedServiceMask></v1:ImplementedServiceMask>
                            <!--Optional:-->
                            <v1:trx_id>${item.TRX_ID[0]}</v1:trx_id>
                        </v1:SendRemoveOfferToAOMRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/SendRemoveOfferToAOM',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']    );
  });
  var kiriman =
    item.TRX_ID[0] + '|' + item.OFFER_ID[0] + '|' + item.MSISDN[0] + '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + 'Success';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/AutoRenewalProductAddRemoveOfferLogs_' +
      moment ().format ('DD-MMM-YY') +
      '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );
}

async function main () {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/GetAOMNotificationARP',
  };
  console.log (moment ().format ('DD-MMM-YY').toUpperCase ());
  const xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                        <v1:GetAOMNotificationARPRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/AutoRenewalProducts/GetAOMNotificationARPRq/v1.0">
                          <v1:COLLECT_DATE>
                            ${moment().format('DD-MMM-YY').toUpperCase()}
                          </v1:COLLECT_DATE>
                        </v1:GetAOMNotificationARPRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/AutoRenewalProducts/services/GetAOMNotificationARP',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT']
    );
    if (result['soapenv:Envelope']['soapenv:Body'][0] !== '') {
      await console.log (
        'banyaknya data ',
        result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'].length
      );
      await console.log (
        'contoh data ',
        result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'][0]
      );
      await result['soapenv:Envelope']['soapenv:Body'][0][
        'OFFER_ELEMENT'
      ].map (async (item, index) => {
        KIRIM_SMS_REMINDER (item);
        // HARUSNYA LOGIC IS_NEED_TO_BE_CHARGED INI :
        // var IS_NEED_TO_BE_CHARGED =
        // moment(item.END_DATE[0])
        // .format('YYYY-MM-DDTHH:mm:ss')
        // .isBetween(moment(), moment().add(10, 'minutes'));
        var IS_NEED_TO_BE_CHARGED = true; // Pakai Moment Bandingkan Waktu
        if (IS_NEED_TO_BE_CHARGED) {
          var HASIL_RESPONSE_CHARGING = await ChargeToINGW (item);
          // if (HASIL_RESPONSE_CHARGING.status === 200) {
             await CreateMemoToAOM (item);
             await KIRIM_SMS_SUCCESS_RENEWAL (item);
          // } else {
             await AddRemoveOffer (item);
             await KIRIM_SMS_GAGAL_RENEWAL (item);
          // }
        }
      });
    } else {
      console.log ('data tidak ada');
    }
  });
}

async function KIRIM_SMS_SUCCESS_RENEWAL (item) {
  await SendSMS (item.MSISDN[0], 'Paket Anda Berhasil di Renewal');
}

async function KIRIM_SMS_GAGAL_RENEWAL (item) {
  await SendSMSGagalRenewal (item.MSISDN[0], 'Paket Anda Gagal di Renewal');
}

async function KIRIM_SMS_REMINDER (item) {
  if (item.hasOwnProperty ('MESSAGE1')) {
    if (item['MESSAGE1'][0] !== '') {
      await SendSMS (item.MSISDN[0], item['MESSAGE1'][0], item['TRX_ID'][0]);
    }
  }
  if (item.hasOwnProperty ('MESSAGE2')) {
    if (item['MESSAGE2'][0] !== '') {
      await SendSMS (item.MSISDN[0], item['MESSAGE2'][0], item['TRX_ID'][0]);
    }
  }
  if (item.hasOwnProperty ('MESSAGE3')) {
    if (item['MESSAGE3'][0] !== '') {
      await SendSMS (item.MSISDN[0], item['MESSAGE3'][0], item['TRX_ID'][0]);
    }
  }
  if (item.hasOwnProperty ('MESSAGE4')) {
    if (item['MESSAGE4'][0] !== '') {
      await SendSMS (item.MSISDN[0], item['MESSAGE4'][0], item['TRX_ID'][0]);
    }
  }
}

main ();
