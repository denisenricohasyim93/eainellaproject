const soapRequest = require ('easy-soap-request');
var SendSMS = require ('./SendSMS');
async function main () {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/OneTimeProduct/services/GetAOMNotificationOTP',
  };
  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                        <v1:GetAOMNotificationOTPRq 	xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/OneTimeProduct/GetAOMNotificationOTPRq/v1.0">
                          <v1:COLLECT_DATE>${moment ()
                                             .subtract (1, 'day')
                                             .format ('DD-MMM-YY')
                                             .toUpperCase ()}</v1:COLLECT_DATE>
                        </v1:GetAOMNotificationOTPRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/OneTimeProduct/services/GetAOMNotificationOTP',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT']
    );
    await console.log (
      'banyaknya data ',
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'].length
    );
    await console.log (
      'contoh data coy ',
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'][0]
    );
    await result['soapenv:Envelope']['soapenv:Body'][0][
      'OFFER_ELEMENT'
    ].map (async (item, index) => {
      var kiriman =
        item.MSISDN[0] +
        '|' + moment() + "|" +
        ' paket anda berhasil di auto teminate';
      var fs = require ('fs');

      var data = kiriman;

      await fs.appendFile (
        './logs/OneTimeProductLogs_' +
          moment ().subtract (1, 'day').format ('DD-MMM-YY').toUpperCase () +
          '.txt',
        data + '\n',
        err => {
          if (err) console.log (err);
          console.log ('Successfully Written to File.');
        }
      );
      if (item.hasOwnProperty ('MESSAGE1')) {
        if (item['MESSAGE1'][0] !== '') {
          SendSMS(item.MSISDN[0], item['MESSAGE1'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE1'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/OneTimeProductLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE2')) {
        if (item['MESSAGE2'][0] !== '') {
          SendSMS(item.MSISDN[0], item['MESSAGE2'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE2'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/OneTimeProductLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE3')) {
        if (item['MESSAGE3'][0] !== '') {
          SendSMS(item.MSISDN[0], item['MESSAGE3'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE3'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/OneTimeProductLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE4')) {
        if (item['MESSAGE4'][0] !== '') {
          SendSMS(item.MSISDN[0], item['MESSAGE4'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE4'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/OneTimeProductLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data,
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
    });
  });
}

main ();
