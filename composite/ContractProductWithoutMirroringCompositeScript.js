const soapRequest = require ('easy-soap-request');
async function SendSMS (msisdn, message, trx_id) {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
  };

  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                      <v1:SendSMSReminderWoMRq xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/CPWithoutMirroring/SendSMSReminderWoMRq/v1.0">
                          <!--Optional:-->
                          <v1:trx_id></v1:trx_id>
                          <v1:msisdn>${msisdn}</v1:msisdn>
                          <v1:smsMessage>${message.split('<spasi>').pop()}</v1:smsMessage>
                      </v1:SendSMSReminderWoMRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/SendSMSReminderWoM',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    // console.log (result);
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body']
    );
  });
  var kiriman =
    trx_id + '|' + msisdn + '|' + moment ().format('YYYY-MM-DD HH:mm:ss') + '|' + message + '|' + 'Success';
  var fs = require ('fs');

  var data = kiriman;

  await fs.appendFile (
    './logs/SendSMSCPWOMLogs_' + moment ().format ('DD-MMM-YY') + '.txt',
    data + '\n',
    err => {
      if (err) console.log (err);
      console.log ('Successfully Written to File.');
    }
  );
}
async function main () {
  var moment = require ('moment');
  const headers = {
    username: 'sigma_jamal',
    password: 'sigma123',
    'Content-Type': 'text/xml;charset=UTF-8',
    soapAction: 'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/GetAOMNotificationWoM',
  };
  const xml = `<soapenv:Envelope 	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
                      <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
                      </soap:Header>
                      <soapenv:Body>
                        <v1:GetAOMNotificationWoMRq 	xmlns:v1="http://www.telkomsel.com/eai/EAISchedulerService/CPWithoutMirroring/GetAOMNotificationWoMRq/v1.0">
                          <v1:COLLECT_DATE>${moment ()
                                             .subtract (1, 'day')
                                             .format ('DD-MMM-YY')
                                             .toUpperCase ()}</v1:COLLECT_DATE>
                        </v1:GetAOMNotificationWoMRq>
                      </soapenv:Body>
                </soapenv:Envelope>`;
  const {response} = await soapRequest (
    'http://10.250.200.87:8011/EAISchedulerService/CPWithoutMirroring/services/GetAOMNotificationWoM',
    headers,
    xml,
    1000000
  );
  var parseString = await require ('xml2js').parseString;
  await parseString (response.body, async function (err, result) {
    var data = await JSON.stringify (
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT']
    );
    await console.log (
      'banyaknya data ',
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'].length
    );
    await console.log (
      'contoh data ',
      result['soapenv:Envelope']['soapenv:Body'][0]['OFFER_ELEMENT'][0]
    );
    await result['soapenv:Envelope']['soapenv:Body'][0][
      'OFFER_ELEMENT'
    ].map (async (item, index) => {
      if (item.hasOwnProperty ('MESSAGE1')) {
        if (item['MESSAGE1'][0] !== '') {
          // SendSMS(item.MSISDN[0], item['MESSAGE1'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE1'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/ContractProductWithoutMirroringLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE2')) {
        if (item['MESSAGE2'][0] !== '') {
          // SendSMS(item.MSISDN[0], item['MESSAGE2'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE2'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/ContractProductWithoutMirroringLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE3')) {
        if (item['MESSAGE3'][0] !== '') {
          // SendSMS(item.MSISDN[0], item['MESSAGE3'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE3'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/ContractProductWithoutMirroringLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data + '\n',
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
      if (item.hasOwnProperty ('MESSAGE4')) {
        if (item['MESSAGE4'][0] !== '') {
          // SendSMS(item.MSISDN[0], item['MESSAGE4'][0])
          var kiriman =
            item.MSISDN[0] +
            '|' + moment() + "|" +
            item['MESSAGE4'][0];
          var fs = require ('fs');

          var data = kiriman;

          await fs.appendFile (
            './logs/ContractProductWithoutMirroringLogs_' +
              moment ()
                .subtract (1, 'day')
                .format ('DD-MMM-YY')
                .toUpperCase () +
              '.txt',
            data,
            err => {
              if (err) console.log (err);
              console.log ('Successfully Written to File.');
            }
          );
        }
      }
    });
  });
}

main ();
