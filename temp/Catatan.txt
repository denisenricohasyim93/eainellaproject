<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema"
        targetNamespace="http://www.telkomsel.com/eai/EAISchedulerService/CPWithMirroring/GetAOMNotificationWMRq/v1.0"
        xmlns:tns="http://www.telkomsel.com/eai/EAISchedulerService/CPWithMirroring/GetAOMNotificationWMRq/v1.0" elementFormDefault="qualified">

    <element name="GetAOMNotificationWMRq">
    
    <complexType>
    	<sequence>
    		<element name="COLLECT_DATE_TIME_START" type="string"  minOccurs="1"></element>
                <element name="COLLECT_DATE_TIME_END" type="string"  minOccurs="1"></element>
    	</sequence>
    </complexType>
    </element>
</schema>

fn-bea:execute-sql(
    "OSB_DEV_EAI","OFFER_ELEMENT",
    "SELECT * FROM EAI_SCHEDULER_TRX WHERE (
        to_date(END_DATE,'YYYY-MM-DD HH24:MI:SS') > to_date(?,'YYYY-MM-DD HH24:MI:SS')
        AND 
        to_date(END_DATE,'YYYY-MM-DD HH24:MI:SS') <= to_date(?,'YYYY-MM-DD HH24:MI:SS')
    ) AND (
        SCHEDULER_CODE = 'ContractMirroring' OR SCHEDULER_CODE = 'ContractMirroring24'
    )
) ORDER BY EAI_SCHEDULER_TRX_ID ASC", $collect_date)